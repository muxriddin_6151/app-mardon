package uz.pdp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.entity.Computer;

public interface ComputerRepository extends JpaRepository<Computer,Integer> {
}
