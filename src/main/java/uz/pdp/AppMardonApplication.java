package uz.pdp;

        import org.springframework.boot.SpringApplication;
        import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppMardonApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppMardonApplication.class, args);
    }

}
